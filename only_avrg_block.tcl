#Create Average block Hierarchical block.
#Tested on Vivado 2019.1 and for Zynq 7020 ZedBoard
#Created by L. Garcia
# The following remote source files are required in the project
#
#    "<git_repo>/VHDL/Average.vhd"
#    "<git_repo>/VHDL/FIFO_ShReg.vhd"


##################################################################
# DESIGN PROCs
##################################################################


# Hierarchical cell: ShReg
proc create_hier_cell_ShReg { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_ShReg() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir I -type clk CLK
  create_bd_pin -dir I -from 15 -to 0 DIN
  create_bd_pin -dir I -from 31 -to 0 DIV
  create_bd_pin -dir O -from 15 -to 0 DOUT
  create_bd_pin -dir O READY_0
  create_bd_pin -dir I -from 0 -to 0 -type rst RST_0

  # Create instance: FIFO_ShReg_0, and set properties
  set block_name FIFO_ShReg
  set block_cell_name FIFO_ShReg_0
  if { [catch {set FIFO_ShReg_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $FIFO_ShReg_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: fifo_generator_0, and set properties
  set fifo_generator_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:fifo_generator:13.2 fifo_generator_0 ]
  set_property -dict [ list \
   CONFIG.Enable_Safety_Circuit {true} \
   CONFIG.Full_Flags_Reset_Value {1} \
   CONFIG.Input_Data_Width {16} \
   CONFIG.Output_Data_Width {16} \
   CONFIG.Reset_Type {Asynchronous_Reset} \
 ] $fifo_generator_0

  # Create port connections
  connect_bd_net -net FIFO_ShReg_0_DOUT [get_bd_pins DOUT] [get_bd_pins FIFO_ShReg_0/DOUT]
  connect_bd_net -net FIFO_ShReg_0_FR_RD_EN [get_bd_pins FIFO_ShReg_0/FR_RD_EN] [get_bd_pins fifo_generator_0/rd_en]
  connect_bd_net -net FIFO_ShReg_0_FW_DIN [get_bd_pins FIFO_ShReg_0/FW_DIN] [get_bd_pins fifo_generator_0/din]
  connect_bd_net -net FIFO_ShReg_0_FW_WR_EN [get_bd_pins FIFO_ShReg_0/FW_WR_EN] [get_bd_pins fifo_generator_0/wr_en]
  connect_bd_net -net FIFO_ShReg_0_READY [get_bd_pins READY_0] [get_bd_pins FIFO_ShReg_0/READY]
  connect_bd_net -net FIFO_ShReg_0_SRST [get_bd_pins FIFO_ShReg_0/SRST] [get_bd_pins fifo_generator_0/rst]
  connect_bd_net -net N_0_1 [get_bd_pins DIV] [get_bd_pins FIFO_ShReg_0/N]
  connect_bd_net -net Net [get_bd_pins CLK] [get_bd_pins FIFO_ShReg_0/CLK] [get_bd_pins fifo_generator_0/clk]
  connect_bd_net -net RST_0_1 [get_bd_pins RST_0] [get_bd_pins FIFO_ShReg_0/RST]
  connect_bd_net -net c_counter_binary_0_Q [get_bd_pins DIN] [get_bd_pins FIFO_ShReg_0/DIN]
  connect_bd_net -net fifo_generator_0_dout [get_bd_pins FIFO_ShReg_0/FR_DOUT] [get_bd_pins fifo_generator_0/dout]
  connect_bd_net -net fifo_generator_0_empty [get_bd_pins FIFO_ShReg_0/FR_EMPTY] [get_bd_pins fifo_generator_0/empty]
  connect_bd_net -net fifo_generator_0_full [get_bd_pins FIFO_ShReg_0/FW_FULL] [get_bd_pins fifo_generator_0/full]
  connect_bd_net -net fifo_generator_0_rd_rst_busy [get_bd_pins FIFO_ShReg_0/rd_rst_busy] [get_bd_pins fifo_generator_0/rd_rst_busy]
  connect_bd_net -net fifo_generator_0_wr_rst_busy [get_bd_pins FIFO_ShReg_0/wr_rst_busy] [get_bd_pins fifo_generator_0/wr_rst_busy]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: Average
proc create_hier_cell_Average { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_Average() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir I -type clk CLK
  create_bd_pin -dir I -from 15 -to 0 DIN
  create_bd_pin -dir O -from 15 -to 0 DOUT
  create_bd_pin -dir I -from 31 -to 0 N
  create_bd_pin -dir O READY
  create_bd_pin -dir I -from 0 -to 0 -type rst RST_0
  create_bd_pin -dir O -from 31 -to 0 RdN

  # Create instance: ADDSUB_0, and set properties
  set block_name ADDSUB
  set block_cell_name ADDSUB_0
  if { [catch {set ADDSUB_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $ADDSUB_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
    set_property -dict [ list \
   CONFIG.DWIDTH {32} \
 ] $ADDSUB_0

  # Create instance: ShReg
  create_hier_cell_ShReg $hier_obj ShReg

  # Create port connections
  connect_bd_net -net ADDSUB_0_R [get_bd_pins ADDSUB_0/B] [get_bd_pins ADDSUB_0/R]
  connect_bd_net -net ADDSUB_0_READY [get_bd_pins READY] [get_bd_pins ADDSUB_0/READY]
  connect_bd_net -net ADDSUB_0_RdN [get_bd_pins RdN] [get_bd_pins ADDSUB_0/RdN]
  connect_bd_net -net FIFO_ShReg_0_DOUT [get_bd_pins DOUT] [get_bd_pins ADDSUB_0/C] [get_bd_pins ShReg/DOUT]
  connect_bd_net -net N_0_1 [get_bd_pins N] [get_bd_pins ADDSUB_0/N] [get_bd_pins ShReg/DIV]
  connect_bd_net -net Net [get_bd_pins CLK] [get_bd_pins ADDSUB_0/CLK] [get_bd_pins ShReg/CLK]
  connect_bd_net -net ShReg_READY_0 [get_bd_pins ADDSUB_0/RST] [get_bd_pins ShReg/READY_0]
  connect_bd_net -net c_counter_binary_0_Q [get_bd_pins DIN] [get_bd_pins ADDSUB_0/A] [get_bd_pins ShReg/DIN]
  connect_bd_net -net xlslice_2_Dout [get_bd_pins RST_0] [get_bd_pins ShReg/RST_0]

  # Restore current instance
  current_bd_instance $oldCurInst
}


# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports

  # Create ports

  # Create instance: Average
  create_hier_cell_Average [current_bd_instance .] Average

  # Create port connections

  # Create address segments


  # Restore current instance
  current_bd_instance $oldCurInst

  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


common::send_msg_id "BD_TCL-1000" "WARNING" "This Tcl script was generated from a block design that has not been validated. It is possible that design <$design_name> may result in errors during validation."

