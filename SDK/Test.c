/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "comblock.h"
#include "xil_io.h"
#include "xparameters.h"


int main()
{
	u32 val[3];
	u32 N=1;
	int i=0;

	xil_printf("Insert window size: ");
	scanf("%d \n\r",&N);
	xil_printf("%d \n\r",N);
//    printf("\e[1;1H\e[2J");
	xil_printf("i, Val, Avrg, MeanDev\n\r");

	cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG2, 1);
	cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG2, 0); //Clear FIFO
	cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG0, N); //Setting N to 10
	cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG1, 5); //Reset high in Average Block
	cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG1, 1); //Reset low in Average Block
	while(1){
		for (int i=0; i<16384; i++){
			val[0]= cbRead(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_IFIFO_VALUE);
			val[1]= cbRead(XPAR_COMBLOCK_1_AXIL_BASEADDR, CB_IFIFO_VALUE);
			val[2]= cbRead(XPAR_COMBLOCK_2_AXIL_BASEADDR, CB_IFIFO_VALUE);

			xil_printf("%d, %d, %d, %d \n \r", i, val[0], val[1], val[2]);
		}
		cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG2, 1);
		cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG2, 0); //Clear FIFO
	}

    cleanup_platform();
    return 0;
}
