----------------------------------------------------------------------------------
--! @author L. G. Garcia
--! @version 0.03 
--! @brief
-- Company: ICTP
-- Engineer: L. G. Garcia  
-- 
-- Create Date: 21.10.2019 11:56:00
-- Design Name: FIFO_ShReg
-- Module Name: FIFO_ShReg - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- Revision:
-- Revision 0.01 - File Created
-- Revision 0.02 - Changed RST to RSTN
-- Revision 0.03 - Add Doxygen style comments.
-- Additional Comments:
--! \f$R=A+B-C\f$
--! \f$RdN=\frac{R}{N}\f$
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ADDSUB is
    Generic(DWIDTH : integer :=16; --! Data width
            OLAT : integer := 0 --! Output latency if it is needed for the division.
    );
    Port(
        CLK : in STD_LOGIC;
        RSTN : in STD_LOGIC;
        EN  : in STD_LOGIC; --! Count enable

        A     : in STD_LOGIC_VECTOR (DWIDTH -1 downto 0); 
        B    : in STD_LOGIC_VECTOR (DWIDTH -1 downto 0);
        C    : in STD_LOGIC_VECTOR (DWIDTH -1 downto 0);
        N    : in STD_LOGIC_VECTOR (DWIDTH -1 downto 0); --! DIVIDED N

        R    : out STD_LOGIC_VECTOR (DWIDTH -1 downto 0); --! RESULT OF A+B-C
        RC   : out STD_LOGIC; --! RESULT CARRY
        RdN  : out STD_LOGIC_VECTOR (DWIDTH -1 downto 0); --! RESULT DIVIDED N

        READY    : out STD_LOGIC --! N 
    );
end ADDSUB;


architecture Behavioral of ADDSUB is

    signal r_sig : signed(DWIDTH  DOWNTO 0); --A+B-C result signal
    signal rdn_sig : integer; --R/N signal
    signal count : integer; --! Count to N to tell is ready. 

    signal n_last : integer; --! n signal to store last value of "N".

begin
    --! @brief
    --! This part of the code do the operation \f$R=A+B-C\f$ and \f$RdN=\frac{R}{N}\f$
    r_sig<='0' & ((signed(A)+signed(B))-signed(C)); --! ADD '0' to take the carry in the result.
    rdn_sig<=to_integer(r_sig)/to_integer(unsigned(N));

    process(CLK, RSTN) --! Output signal process
    begin
        if RSTN='0' then
            R<=(OTHERS=>'0');
            RC<='0';
            RdN<=(Others=>'0');
        elsif rising_edge(CLK) then
                R<=std_logic_vector(r_sig(DWIDTH -1 DOWNTO 0));
                RC<=r_sig(DWIDTH);
                RdN<=std_logic_vector(to_signed(rdn_sig, DWIDTH));             
        end if;
    end process;

    --! Counter process and count enable for "READY" signal
    process(CLK, RSTN, EN, N)
    begin
        IF RSTN = '0' THEN
            count<=0;
            READY<='0';
            n_last<=0;
        ELSIF rising_edge(CLK) then
            IF EN = '1' then
                IF n_last /= to_integer(unsigned(N)) then --! If N changes, counter restarts
                    n_last<=to_integer(unsigned(N));
                    count<=0;
                    READY<='0';
                ELSIF count >= to_integer(unsigned(N)) + OLAT THEN --! Rise "Ready" signal when count + latency is reached. 
                    READY<='1';
                ELSE --! Counter Increase
                    READY<='0';
                    count<=count+1;
                END IF;  
            END IF;
        END IF;
    end process;

end Behavioral;