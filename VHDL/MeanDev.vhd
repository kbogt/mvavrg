----------------------------------------------------------------------------------
-- Company: ICTP
-- Engineer: L. G. Garcia  
-- 
-- Create Date: 21.10.2019 11:56:00
-- Design Name: FIFO_ShReg
-- Module Name: FIFO_ShReg - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- R=A+B-C
-- RdN=R/N
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MeanDev is
    Generic(DWIDTH : integer :=16);
    Port(
        CLK : in STD_LOGIC;
        RST : in STD_LOGIC;

        A     : in STD_LOGIC_VECTOR (DWIDTH -1 downto 0);
        B    : in STD_LOGIC_VECTOR (DWIDTH -1 downto 0);

        R    : out STD_LOGIC_VECTOR (DWIDTH -1 downto 0);
        RC   : out std_logic
    );
end MeanDev;


architecture Behavioral of MeanDev is

    signal r_sig : signed(DWIDTH DOWNTO 0);


begin

R<=std_logic_vector(r_sig(DWIDTH -1 DOWNTO 0));
RC<=r_sig(DWIDTH);


    process(CLK, RST)
    begin
        if RST='0' then
            r_sig<=(OTHERS=>'0');
        elsif rising_edge(CLK) then
            r_sig<=abs(signed('0'&A)- signed('0'&B));
        end if;
    end process;

end Behavioral;