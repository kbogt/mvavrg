----------------------------------------------------------------------------------
--! @author L. G. Garcia
--! @version 0.03 
--! @brief 
-- Company: ICTP
-- Engineer: L. G. Garcia  
-- 
-- Create Date: 21.10.2019 11:56:00
-- Design Name: FIFO_ShReg
-- Module Name: FIFO_ShReg - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Revision 0.02 - Add DVALID signal
-- Revision 0.03 - Change RST to RSTN and add SET signal to change N by register.
-- Revision 0.04 - Fix n_max issue when set =1
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FIFO_ShReg is
    Generic(DWidth : integer :=16;
            FIFO_DEPTH : integer := 32;
            Latency : integer :=1);
    Port (
    --! NATIVE PORTS
    CLK         : IN STD_LOGIC;
    RSTN        : IN STD_LOGIC;
    SET         : IN STD_LOGIC;
    DIN         : IN STD_LOGIC_VECTOR (DWidth-1 DOWNTO 0);
    DVALID      : IN STD_LOGIC;
    DOVALID     : OUT STD_LOGIC;
    DOUT        : OUT STD_LOGIC_VECTOR (DWidth-1 DOWNTO 0);
    N           : IN STD_LOGIC_VECTOR  (FIFO_DEPTH -1 DOWNTO 0);
    READY       : OUT STD_LOGIC;

    --! FIFO PORTS
        wr_rst_busy : IN STD_LOGIC;
        rd_rst_busy : IN STD_LOGIC;
        FW_FULL     : IN STD_LOGIC;
        FW_DIN      : OUT   STD_LOGIC_VECTOR (DWidth-1 downto 0);
        FW_WR_EN    : OUT   STD_LOGIC;
        FR_EMPTY    : IN    STD_LOGIC;
        FR_DOUT     : IN    STD_LOGIC_VECTOR (DWidth-1 downto 0);
        FR_RD_EN    : OUT   STD_LOGIC;
        SRST        : OUT   STD_LOGIC
    );
end FIFO_ShReg;

architecture Behavioral of FIFO_ShReg is
type state is (RST_STATE, RST_WAIT, WAIT_STATE, READY_STATE, RUN_STATE);
signal c_state, n_state : state;

signal n_max, n_count : unsigned (FIFO_DEPTH -1 DOWNTO 0);

signal FR_RD_EN_SIG, FW_WR_EN_SIG : std_logic;

begin

DOVALID<=DVALID;
FR_RD_EN<=FR_RD_EN_SIG and DVALID;
FW_WR_EN<=FW_WR_EN_SIG and DVALID;

process(CLK, RSTN, SET)
begin
    if RSTN ='0' THEN
        c_state<=RST_STATE;
    elsif rising_edge(CLK) then 
            if SET = '1' then
                c_state<=RST_STATE;
            else
                c_state<=n_state;
            end if;
    end if;
end process;

process(CLK, RSTN, c_state, N)
begin
    if RSTN='0' THEN
        n_max<=unsigned(N)-to_unsigned(2+Latency,32);
        n_count<=(others=>'0');
    elsif rising_edge(CLK) then
        if SET = '1' then
            n_max<=unsigned(N)-to_unsigned(2+Latency,32);
            n_count<=(others=>'0');
        ELSIF DVALID ='1' then
            if (c_state = WAIT_STATE) and (n_count<=n_max) then
                n_count<=n_count+1;
            end if;
        end if;
    end if;            
end process;



process(c_state)
begin
    if c_state = RST_STATE then
        FR_RD_EN_SIG<='0';
        FW_WR_EN_SIG<='0';
        FW_DIN<=(others=>'0');
        DOUT<=(others =>'0');
        SRST<='1';
        READY<='0';
    elsif c_state = RST_WAIT then
        FR_RD_EN_SIG<='0';
        FW_WR_EN_SIG<='0';
        FW_DIN<=(others=>'0');
        DOUT<=(others =>'0');
        SRST<='0';
        READY<='0';  
    elsif c_state = WAIT_STATE then
        FR_RD_EN_SIG<='0';
        FW_WR_EN_SIG<='1';
        FW_DIN<=DIN;
        DOUT<=(others =>'0');
        SRST<='0';
        READY<='1';
    elsif c_state = READY_STATE then
        FR_RD_EN_SIG<='1';
        FW_WR_EN_SIG<='1';
        FW_DIN<=DIN;
        DOUT<=(others =>'0');
        SRST<='0';
        READY<='1';
    elsif c_state = RUN_STATE then
        FR_RD_EN_SIG<='1';
        FW_WR_EN_SIG<='1';
        FW_DIN<=DIN;
        DOUT<=FR_DOUT;
        SRST<='0';
        READY<='1';
    end if;
end process;

process(c_state, n_count, CLK, wr_rst_busy, rd_rst_busy)
begin
    case (c_state) is 
        when RST_STATE =>
            n_state<=RST_WAIT;
        when RST_WAIT =>
            if (wr_rst_busy ='0') and (rd_rst_busy ='0') then
                n_state<=WAIT_STATE;
            else
                n_state<=RST_WAIT;
            end if;
        when WAIT_STATE =>
            if n_count > n_max then
                n_state<=READY_STATE;
            else
                n_state<=WAIT_STATE;
            end if;
        when READY_STATE =>
            n_state<=RUN_STATE;        
        when RUN_STATE =>
            n_state<=RUN_STATE;
        when others =>
            n_state<=RST_STATE;
    end case;
end process;

end Behavioral;
